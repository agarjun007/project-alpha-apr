from django.template import loader
from .models import Task
from django.contrib.auth.decorators import login_required
from .forms import TaskForm
from django.urls import reverse
from django.shortcuts import redirect
from django.http import HttpResponse

@login_required(login_url='/accounts/login/')
def create_task(request):
    if request.method == 'POST':
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('list_projects'))
            
        else:
            form = TaskForm()
            context={'form':form}
            html_template = loader.get_template('create_project.html')
            return HttpResponse(html_template.render(context, request))
    else:
        form = TaskForm()
        context={'form':form}
        html_template = loader.get_template('create_project.html')
        return HttpResponse(html_template.render(context, request))

@login_required(login_url='/accounts/login/')
def show_my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context={'tasks':tasks}
    html_template = loader.get_template('my_tasks.html')
    return HttpResponse(html_template.render(context, request))
