from django.http import HttpResponse
from django.template import loader
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm
from django.urls import reverse
from django.shortcuts import redirect

@login_required(login_url='/accounts/login/')
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context={'projects':projects}
    html_template = loader.get_template('list_projects.html')
    return HttpResponse(html_template.render(context, request))

@login_required(login_url='/accounts/login/')
def show_project(request,id):
    project = Project.objects.get(id=id)
    context={'project':project}
    html_template = loader.get_template('project_detail.html')
    return HttpResponse(html_template.render(context, request))

@login_required(login_url='/accounts/login/')
def create_project(request):
    if request.method == 'POST':
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect(reverse('list_projects'))
            
        else:
            form = ProjectForm()
            context={'form':form}
            html_template = loader.get_template('create_project.html')
            return HttpResponse(html_template.render(context, request))
    else:
        form = ProjectForm()
        context={'form':form}
        html_template = loader.get_template('create_project.html')
        return HttpResponse(html_template.render(context, request))
